from flask import Flask, request
from csv_handler import Users
from http import HTTPStatus
import json
import csv
import os


def create_app():

    app = Flask(__name__)

    @app.route('/signup', methods=['POST'])
    def signup():

        if request.method == 'POST':
            data = request.get_json()

            if None in [ 
                data.get('age'), 
                data.get('password'), 
                data.get('email'), 
                data.get('name')
            ]:

                return json.dumps({ 
                    "status" : 400,
                    "error_code" : "400 - Bad Request", 
                    "more_info" : "https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/400" 
                }), 400

            registered_users = Users.get_users()

            for user in registered_users:
                if user['email'] == data['email']:
                    return json.dumps({}), 422

            data.update({"id": Users.next_avaliable_id()})
            Users.create_user(data)

        return json.dumps(data), HTTPStatus.CREATED


    @app.route('/login', methods=['POST'])
    def login():

        if request.method == 'POST':
            data = request.get_json()

            with open('users.csv', 'r') as reading:
                reader = csv.DictReader(reading, delimiter=',')
                users = [ user for user in reader if user['email'] == data['email'] and user['password'] == data['password']]

                if users:
                    return json.dumps({
                        "name": users[0]['name'],
                        "email": users[0]['email'],
                        "age": users[0]['age']
                    })

                return {}, 422


    @app.route('/profile/<int:user_id>', methods=['PATCH'])
    def update_user(user_id):

        if request.method == 'PATCH':

            data = request.get_json()
            valid_keys = ('age', 'email', 'password', 'name',)
            response = ''
            status = 0

            if all(item in list(valid_keys) for item in list(data.keys())):

                user = Users.get_user_by_id(user_id)
                if user:
                    Users.delete_user(user_id)
                    user.update(data)
                    Users.create_user(user)

                    response = {
                        "name": user['name'],
                        "email": user['email'],
                        "password": user['password'],
                        "age": int(user['age'])
                    }

                    status = 200

                else:     
                    response =  {
                        "status" : 404,
                        "error_code" : "404 - Not Found"
                    }
                    status = 404

            else: 
                response = {
                    "status" : "422 - UNPROCESSABLE_ENTITY",
                    "error_code" : 422
                }

                status = HTTPStatus.UNPROCESSABLE_ENTITY

        return json.dumps(response), status


    @app.route('/profile/<int:user_id>', methods=['DELETE'])
    def delete_user(user_id):

        if request.method == 'DELETE':
            Users.delete_user(user_id)

            return json.dumps({}), 204


    @app.route('/users', methods=['GET'])
    def all_users():

        users = Users.get_users()

        for user in users:
            if 'password' in user:
                del user['password']

        return json.dumps(users), 200
    
    return app


app = create_app()
