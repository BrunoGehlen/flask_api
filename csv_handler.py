import csv
import os

class Users():
    
    file_name = 'users.csv'
    field_names = ['id','name','email','password','age']

    @staticmethod
    def write_list(dict_list):

        with open(Users.file_name, 'a') as file:
            writer = csv.DictWriter(file, fieldnames=Users.field_names)

            if os.stat(Users.file_name).st_size == 0:
                writer.writeheader()

            if dict_list:
                for dict_ in dict_list:
                    writer.writerow(dict_)
    
    @staticmethod 
    def create_user(user):

        with open(Users.file_name, 'a') as file:
            writer = csv.DictWriter(file, fieldnames=Users.field_names)

            if os.stat(Users.file_name).st_size == 0:
                writer.writeheader()

            writer.writerow(user)

    @staticmethod  
    def delete_user(user_id):
        
        users = []
        with open(Users.file_name, 'r') as file:
            reader = csv.DictReader(file, delimiter=',')
            users = [ user for user in reader if int(user['id']) != user_id ]

        open(Users.file_name, 'w').truncate(0)
        Users.write_list(users)
        return None
    
    @staticmethod 
    def get_users():
        
        users = []

        with open(Users.file_name, 'r') as file:
            reader = csv.DictReader(file, delimiter=',')
            users = [ user for user in reader ]
        
        return users

    @staticmethod 
    def next_avaliable_id():

        next_id = 0
        users = []

        with open(Users.file_name, 'r') as file:
            reader = csv.DictReader(file, delimiter=',')
            ids = [ user['id'] for user in reader ]

        if ids:
            ids.sort(key=lambda id: int(id))
            next_id = int(ids[-1])

        return next_id + 1

    @staticmethod 
    def get_user_by_id(user_id):

        with open(Users.file_name, 'r') as file:
            reader = csv.DictReader(file, delimiter=',')
            user = [ user for user in reader if int(user['id']) == user_id ]
        
        if user:
            return user[0]
        
        else:
            return None

