from flask import Flask
from pytest import fixture
from json import loads
from flask import Flask as flask
from app import create_app


@fixture
def app():
    return create_app()

@fixture
def client(app: Flask):
    return app.test_client()

def test_signup(client):

    given = {
        "name":"John Doe",
        "email":"johndoe@gmail.com",
        "password":"johndoefoo",
        "age":25
        }

    response = client.post('/signup', json=given)
    
    assert response.status_code == 201

def test_login(client):
    
    given={
        "email":"johndoe@gmail.com",
        "password":"johndoefoo",
        }
    
    response = client.post('/login', json=given)
    
    assert response.status_code == 200

def test_profile_1(client):
    
    given={
        "email":"johndoe@gmail.com",
        }

    response = client.patch('/profile/1', json=given)
    
    assert response.status_code == 200

def test_profile_2(client):
    
    response = client.delete('/profile/2')
    
    assert response.status_code == 204

def test_users(client):
    
    response = client.get('/users')
    
    assert response.status_code == 200
